from symbols import *
from typing import Iterator, List
import random

class GrammarRule:
    def __init__(self, symbol: NonTerminalSymbol, *symbols: Symbol):
        self.symbol = symbol
        self.symbols = list(symbols)
    
    def __str__(self):
        symbols_strings = map(lambda x: str(x), self.symbols)
        symbols_string = ' '.join(symbols_strings)
        return f"{self.symbol} = {symbols_string}"
    
    def replace_symbol(self, symbol, other_symbol):
        if self.symbol == symbol:
            self.symbol = other_symbol
        
        for index, item in enumerate(self.symbols):
            if symbol == item:
                self.symbols[index] = other_symbol
    
    def __eq__(self, other):
        if not isinstance(other, GrammarRule):
            return False
        if self.symbol != other.symbol:
            return False
        if len(self.symbols) != len(other.symbols):
            return False
        for symbol, other_symbol in zip(self.symbols, other.symbols):
            if symbol != other_symbol:
                return False
        return True

class Grammar:
    START_SYMBOL = NonTerminalSymbol('S')

    def __init__(self, *rules: GrammarRule):
        self.rules = list(rules)
    
    def add_rule(self, rule: GrammarRule):
        self.rules.append(rule)

    def select_rules(self, symbol: NonTerminalSymbol) -> List[GrammarRule]:
        selected_rules = filter(lambda rule: rule.symbol == symbol, self.rules)
        return list(selected_rules)

    def replace_symbol(self, symbol, other_symbol):
        for rule in self.rules:
            rule.replace_symbol(symbol, other_symbol)
    
    def remove_rule(self, rule):
        self.rules.remove(rule)

    def find_end_rule(self, symbol, end_symbol):
        for rule in self.rules:
            if (len(rule.symbols) == 1 and
                rule.symbol == symbol and
                rule.symbols[0] == end_symbol):
                return rule
        return None

    def find_partial_rules(self, symbol):
        for rule in self.rules:
            if (len(rule.symbols) == 2 and
                rule.symbols[0] == symbol and
                isinstance(rule.symbols[1], NonTerminalSymbol)):
                yield rule

    def find_tail_rules(self):
        for rule in self.rules:
            if (len(rule.symbols) == 2 and 
                isinstance(rule.symbols[0], TerminalSymbol) and 
                isinstance(rule.symbols[1], TerminalSymbol)):
                yield rule
        return None

    def find_rule(self, rule):
        for rule_ in self.rules:
            if rule_ == rule:
                return rule_
        return None

    def clean_rule(self, rule):
        found_rules = list(filter(lambda local_rule: local_rule == rule, self.rules))
        if len(list(found_rules)) != 1:
            for found_rule in found_rules:
                self.rules.remove(found_rule)
            self.rules.append(rule)

    def generate(self):
        symbols = self.generate_symbol(Grammar.START_SYMBOL)

        generated_string = ''.join(map(lambda x: x.name, list(symbols)))
        return generated_string

    def generate_symbol(self, symbol: Symbol) -> Iterator[Symbol]:
        if isinstance(symbol, TerminalSymbol):
            yield symbol
        elif isinstance(symbol, NonTerminalSymbol):
            rules = self.select_rules(symbol)
            rule = random.choice(rules)
            for rule_symbol in rule.symbols:
                for symbol in self.generate_symbol(rule_symbol):
                    yield symbol
        else:
            return []