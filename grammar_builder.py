from grammar import *
from typing import Iterator, List

class Entry:
    def __init__(self, prefix, symbol):
        self.prefix = prefix
        self.symbol = symbol

class GrammarBuilder:
    def __init__(self):
        self.index = 0
        self.entries = []
        self.grammar = Grammar()

    def find_entry(self, prefix):
        for entry in self.entries:
            if entry.prefix == prefix:
                return entry
        return None

    def add_entry(self, prefix, symbol, rule):
        self.entries.append(Entry(prefix, symbol))
        self.grammar.add_rule(rule)

    def build(self, strings) -> Grammar:
        strings = sorted(strings, key=lambda s: len(s), reverse=True)

        length = len(strings[0])
        for s in strings:
            self.build_string(s, longest=length == len(s))

        self.generalize()

        self.clean()

        return self.grammar

    def build_string(self, st, longest=False):
        if longest:
            offset = -1
        else:
            offset = 0
        current_non_terminal = Grammar.START_SYMBOL
        for i in range(1, len(st) + offset):
            prefix = st[:i]
            entry = self.find_entry(prefix)
            if entry == None:
                current_non_terminal, prev_non_terminal = self.non_terminal(), current_non_terminal
                terminal = TerminalSymbol(st[i - 1])
                rule = GrammarRule(prev_non_terminal, terminal, current_non_terminal)
                self.add_entry(prefix, current_non_terminal, rule)
            else:
                current_non_terminal = entry.symbol

        suffix = st[(offset - 1):]
        terminals = map(lambda c: TerminalSymbol(c), suffix)
        self.grammar.add_rule(GrammarRule(current_non_terminal, *terminals))

    def generalize(self):
        tail_rules = list(self.grammar.find_tail_rules())
        for tail_rule in tail_rules:
            for partial_rule in self.grammar.find_partial_rules(tail_rule.symbols[0]):
                end_rule = self.grammar.find_end_rule(partial_rule.symbols[1], tail_rule.symbols[1])
                if end_rule != None:
                    self.grammar.replace_symbol(tail_rule.symbol, partial_rule.symbol)
                    self.grammar.remove_rule(tail_rule)
                    break

    def clean(self):
        rules = list(self.grammar.rules)
        for rule in rules:
            self.grammar.clean_rule(rule)

    def non_terminal(self):
        self.index += 1
        return NonTerminalSymbol(f"A{self.index}")