#!/usr/bin/env python3
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import random
import time
from grammar import *
from grammar_builder import *

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.strings_text_edit = QTextEdit()
        self.strings_text_edit.setAlignment(Qt.AlignVCenter)

        self.build_button = QPushButton()
        self.build_button.setText("Build")
        self.build_button.clicked.connect(lambda event: self.on_build())

        self.rules_text_edit = QTextEdit()
        self.rules_text_edit.setAlignment(Qt.AlignVCenter)
        self.rules_text_edit.setReadOnly(True)

        self.generate_button = QPushButton()
        self.generate_button.setText("Generate")
        self.generate_button.clicked.connect(lambda event: self.on_generate())

        self.generated_line = QLineEdit()

        vb = QVBoxLayout()
        vb.addWidget(self.strings_text_edit)
        vb.addWidget(self.build_button)
        vb.addWidget(self.rules_text_edit)
        vb.addWidget(self.generate_button)
        vb.addWidget(self.generated_line)
        
        self.setCentralWidget(QWidget())
        self.centralWidget().setLayout(vb)
        self.show()

    def on_generate(self):
        generated_text = self.grammar.generate()
        self.update_generated_text(generated_text)

    def on_build(self):
        strings = self.strings_text_edit.toPlainText().split('\n')
        grammar = GrammarBuilder().build(strings)
        self.update_grammar_view(grammar)

    def update_grammar_view(self, grammar: Grammar):
        self.grammar = grammar
        rules = map(lambda x: str(x), grammar.rules)
        self.rules_text_edit.setText('\n'.join(rules))

    def update_generated_text(self, text):
        self.generated_line.setText(text)

if __name__ == '__main__':
    app = QApplication([])
    window = MainWindow()
    app.exec_()