class Symbol:
    def __init__(self, name):
        super().__init__()
        self.name = name
        
    def __str__(self):
        return self.name

class TerminalSymbol(Symbol):
    def __init__(self, name):
        super().__init__(name)
    
    def __eq__(self, other):
        return isinstance(other, TerminalSymbol) and (self.name == other.name)

class NonTerminalSymbol(Symbol):
    def __init__(self, name):
        super().__init__(name)

    def __eq__(self, other):
        return isinstance(other, NonTerminalSymbol) and (self.name == other.name)